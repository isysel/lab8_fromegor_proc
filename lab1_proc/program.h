#ifndef __program__
#define __program__

#include <string>
#include <fstream>
using namespace std;

namespace simple_shapes
{
	struct Aphorism {
		char author[256];
		
	};
	void In(Aphorism &a, ifstream &ifst);
	void Out(Aphorism &a, ofstream &ofst);


	struct Proverb {
		char country[256];
		
	};
	void In(Proverb &p, ifstream &ifst);
	void Out(Proverb &p, ofstream &ofst);
	
	struct Riddle {
		char answer[256];
		
	};
	void In(Riddle &r, ifstream &ifst);
	void Out(Riddle &r, ofstream &ofst);
	
	

	// ���������, ���������� ��� ��������� ������
	struct Shape
	{
		char text[256];
		int assessment;
		
		// �������� ������ ��� ������ �� �����
		enum key { APHORISM, PROVERB, RIDDLE};
		key k; // ����
			   // ������������ ������������
		union { // ���������� ���������
			Aphorism a;
			Proverb p;
			Riddle r;
		};
	};
	
	int Count(Shape &s);
	Shape* In(ifstream &ifst);
	void Out(Shape &s, ofstream &ofst);

	struct  Node
	{
		Shape* x;
		Node *next;
		Node *prev;
	};

	struct List
	{
		Node *head, *tail; //������ ������� � ��� ��� ���������
		int size; //����� ��������� � ������
	};

	void Init(struct List &lst);
	void Clear(struct List &lst);
	void In(struct List &lst, ifstream &ifst);
	void Out(struct List &lst, ofstream &ofst);
	bool Compare(Shape *first, Shape *second);
	void Sort(struct List &lst);
	void OutAphorisms(struct list &lst, ofstream &ofst);
	void MultiMethod(List &lst, ofstream &ofst);
}
#endif
